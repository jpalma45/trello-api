'use strict'

var express = require('express');
var bodyParser = require('body-parser');
const cors = require('cors')

var app = express();

//middlewares
app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

//cors
app.use(cors());


//exportar
module.exports = app;
