'use strict';

const Trello = require("node-trello");
const service = require("../services/actions.service");
const ActionModel = require('../models/action.model');
const api_KEY_TOKEN = new Trello(service.KEY(), service.TOKEN());

function home(req, res) {
    res.status(200).send({
        message: 'Actions'
    });
}

async function actions(req, res){
    const ActionId = req.params.id;
    var response;

   await api_KEY_TOKEN.get(`/1/actions/${ActionId}`,function(err, data){
        
        if (err) res.status(500).send({ message: 'Error en la petición' });
        
        if (err) throw err;
        
        response = service.action(data);

        saveActions(response, res);

    });
}

async function saveActions(data, res) {

    var actionModel = new ActionModel();

    for(let actions of data){
        actionModel.set(actions);
    }

    await actionModel.save((err, actionStore) => {
       if (err) return res.status(500).send({
           message: 'Error en el ingreso de la informacion'
       });

       if (actionStore) {
          return res.status(200).send({
               Action: actionStore
           });
       } else {
         return res.status(404).send({
            message: 'no se  ingreso de la informacion'
         });
       }
   });
    // console.log(data);
    
}

function actionIdBoard (req, res) {
    const ActionId = req.params.id;
    var response;

    api_KEY_TOKEN.get(`/1/actions/${ActionId}/board`, function(err, data){
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;  
        
        response = service.actionIdBoard(data);

        res.status(200).send({
            actionBoard: response
        });
    });
}

function actionCard (req, res) {
    const ActionId = req.params.id;
    var response = [];

    api_KEY_TOKEN.get(`/1/actions/${ActionId}/card`, function(err, data){
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        response.push(service.actionCard(data));

        res.status(200).send({
            actionCard: response
        });
    });
}

function actionDisplayCard (req, res) {
    const ActionId = req.params.id;
    
    api_KEY_TOKEN.get(`/1/actions/${ActionId}/display`, function(err, data){
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        res.status(200).send({
            actionDisplay: data
        });
    });
}

module.exports = {
   home,
   actions,
   actionIdBoard,
   actionCard,
   actionDisplayCard
};