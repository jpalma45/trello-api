'use strict';

// const Trello = require("trello");
const Trello = require("node-trello");
const service = require("../services/card.service");
const CardModel = require('../models/card.model');
// const api = new Trello(process.env.KEY, process.env.TOKEN);
const api_KEY_TOKEN = new Trello(service.KEY(), service.TOKEN());

// const boardId = "55411859be21b8ad7dcd4c78";
// const listId = "5ac61a2e05fae550b0d45c94";
// const cardId = "560bf4dd7139286471dc009c";
// const memberId = "50095233f62adbe04d935195";
// cardId 5914b27920508fcb6bfd525f


function home(req, res) {
    res.status(200).send({
        message: 'Cards'
    });
} 

async function InformationCard(req, res) {
    const CardId = req.params.id;
    var response = [];

    await api_KEY_TOKEN.get(`/1/cards/${CardId}`,function(err, data){
        
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        response = service.InformationCard(data);

        // console.log(response);
        

        // res.status(200).send({
        //   cardinfo: response   
        // });

        saveInformationCard(response, res);
    });
}

async function saveInformationCard(data, res) {
    
    var cardModel = new CardModel();

    cardModel.set(data);

    await cardModel.save((err, cardStore) => {
        if (err) return res.status(500).send({
            message: 'Error en el ingreso de la informacion'
        });

        if (cardStore) {
            return res.status(200).send({
                cardinfo: cardStore
             });
         } else {
           return res.status(404).send({
              message: 'no se  ingreso de la informacion'
           });
         }
    })
}



function CardBoard (req, res){
    const cardId = req.params.id;
    const response = [];

    api_KEY_TOKEN.get(`/1/cards/${cardId}/board`, function(err, data){
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        response.push(service.CardBoard(data));

        res.status(200).send({
            boardCard: response
        });
    });
}

function checkItemStatesCard (req, res){
    const cardId = req.params.id;

    api_KEY_TOKEN.get(`/1/cards/${cardId}/checkItemStates`, function(err, data){
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        res.status(200).send({
            checkItemStatesCard: data
        });
    });
}

function checklistsCard (req, res){
    const cardId = req.params.id;
    var response = [];

    api_KEY_TOKEN.get(`/1/cards/${cardId}/checklists`, (err, data) => {
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        for (let index = 0; index < data.length; index++) {
            response.push(service.checklistsCard(data[index]));
          }
  
        res.status(200).send({
            checklistsCard: response
        });     
    });
}

module.exports = {
    home,
    InformationCard,
    CardBoard,
    checkItemStatesCard,
    checklistsCard
};