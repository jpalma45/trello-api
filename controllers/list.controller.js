'use strict';

const Trello = require("node-trello");
const service = require("../services/list.service");
const ListModel = require('../models/list.model');
const axios = require('axios');
const api_KEY_TOKEN = new Trello( service.KEY() , service.TOKEN());

function home(req, res) {
    res.status(200).send({
        message: 'Lists'
    });
}

// var request = require("request");

// var options = {
//   method: 'GET',
//   url: 'https://api.trello.com/1/lists/5ac61a2e05fae550b0d45c94',
//   qs: {
//     fields: 'name,closed,idBoard,pos',
//     key: '24308d2ea050461c8e189707086a6915',
//     token: '4c22fa6749c5d3f7fea358c4ec980739bb9e2d01cad596031d0b6d3c250b2310'
//   }
// };

// request(options, function (error, response, body) {
//   if (error) throw new Error(error);

//   console.log(body);
// });

// axios.get('https://api.trello.com/1/lists/5ac61a2e05fae550b0d45c94',{
//     params: {
//       fields: 'name,closed,idBoard,pos',
//       key: '24308d2ea050461c8e189707086a6915',
//       token: '4c22fa6749c5d3f7fea358c4ec980739bb9e2d01cad596031d0b6d3c250b2310'
//     }
// }).then( res => console.log(res));

// axiosf();

// async function axiosf(){
//     try {
//         const res = await axios.get('https://api.trello.com/1/lists/5ac61a2e05fae550b0d45c94',{
//             params: {
//               fields: 'name,closed,idBoard,pos',
//               key: '24308d2ea050461c8e189707086a6915',
//               token: '4c22fa6749c5d3f7fea358c4ec980739bb9e2d01cad596031d0b6d3c250b2310'
//             }});
        
//             console.log(res);
//     } catch (error) {
//         console.error(error);
//     }
// }


// GetlistIdInfo();

async function GetlistIdInfo(req, res) {
    const ListId = '5ac61a2e05fae550b0d45c94';
    const field = 'name,closed,idBoard,pos';

    await api_KEY_TOKEN.get(`/1/lists/${ListId}/${field}`,function(err, data){
        
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        console.log(data);

    //    saveList(data, res);
    });
}

async function saveList(data, res) {
    
    var listModel = new ListModel();

    listModel.set(data);

    await listModel.save((err, listStore) =>{

        if(err) return res.status(500).send({
            message: 'Error en el ingreso de la informacion'
        });

        if (listStore) {
            return res.status(200).send({
                List: listStore
            });
        } else {
            return res.status(404).send({
                message: 'No se pudo ingresar la informacion'
            });
        }
    });
}

// GetlistFieldInfo();


function GetlistFieldInfo(req, res) {
    const ListId = '5ac61a2e05fae550b0d45c93';
    const field = 'id,name,closed';

    api_KEY_TOKEN.get(`/1/boards/${ListId}/lists?fields=all&filter=all`,function(err, data){
        
        if (err) res.status(500).send({ message: 'Error en la petición' });

        if (err) throw err;

        console.log(data);

        // res.status(200).send({
        //    GetlistField: data
        // });
    });
}

module.exports = {
//    home,
//    GetlistIdInfo,
//    GetlistFieldInfo
}