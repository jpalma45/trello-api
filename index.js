'use strict'

const mongoose = require('mongoose');
const app = require('./app');
const morgan = require('morgan');

//settings
app.set('port', process.env.PORT || 3800);

//middleware
app.use(morgan('dev'));

//connect to database mongoDB
mongoose.Promise = global.Promise;


mongoose.connect('mongodb://localhost:27017/trello',
        { 
          useNewUrlParser: true, 
          useUnifiedTopology: true, 
          useCreateIndex: true
        }
        )
        .then(() => {
            console.log('Connect to database');
          
        // starting the server
          app.listen(app.get('port'), () => {
            console.log(`server on port ${app.get('port')}`); 
          });

        })
        .catch(err => console.log(err));

// mongoose.set('useFindAndModify', false);
// mongoose.connect('mongodb+srv://jpalma:1234@trellobd-ellk5.mongodb.net/test?retryWrites=true&w=majority',
// {
//   useCreateIndex: true,
//   useNewUrlParser: true
// })
//  .then(db => console.log('DB connect'))
//   .catch(err => console.log(err));