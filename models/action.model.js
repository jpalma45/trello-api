'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActionSchema = Schema({
  id: String,
  idMemberCreator: String,
  type: String,
  date: Date,
  translationKey: String,
  entities: Object,
});

module.exports = mongoose.model('Action', ActionSchema);