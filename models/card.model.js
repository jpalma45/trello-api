'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let CardSchema = new Schema({
    id: String,
    badges: Object,
    closed: Boolean,
    dueComplete: Boolean,
    dateLastActivity: Date
});

module.exports = mongoose.model('Card', CardSchema);