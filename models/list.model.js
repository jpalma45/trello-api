'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let ListSchema = Schema({
    id: String,
    name: String,
    closed: Boolean,
    idBoard: String,
    pos: Number
});

module.exports = mongoose.model('Lits', ListSchema);