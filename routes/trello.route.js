// 'use strict'

// var express = require('express');
// var api = express.Router();
// var CardsController = require('../controllers/card.controller');
// var ActionsController = require('../controllers/actions.controller');
// var ListsController = require('../controllers/list.controller');

// // CARDS
// api.get('/home/card', CardsController.home); // Revisar
// api.get('/cardinfo/:id', CardsController.InformationCard);
// api.get('/cardboard/:id', CardsController.CardBoard);
// api.get('/checkItemcard/:id', CardsController.checkItemStatesCard); // id = IdCard
// api.get('/checklistscard/:id', CardsController.checklistsCard); // id = IdCard


// // ACTIONS
// api.get('/home/action', ActionsController.home);
// api.get('/action/:id', ActionsController.actions);
// api.get('/actionIdBoard/:id', ActionsController.actionIdBoard);
// api.get('/actioncard/:id', ActionsController.actionCard);
// api.get('/displaycard/:id', ActionsController.actionDisplayCard);  // Revisar

// // LISTS
// api.get('/home/list', ListsController.home);
// api.get('/getlistid/:id', ListsController.GetlistIdInfo);
// api.get('/getlistfield/:id/:field', ListsController.GetlistFieldInfo);
// api.get('/getlistactions/:id', ListsController.GetlistActionsInfo);
// api.get('/getlistcards/:id', ListsController.GetlistCardsInfo);


// module.exports = api;