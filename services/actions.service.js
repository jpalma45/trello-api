'use strict'

function KEY() {
    return "24308d2ea050461c8e189707086a6915";
}

function TOKEN() {
    return "4c22fa6749c5d3f7fea358c4ec980739bb9e2d01cad596031d0b6d3c250b2310";
}

function action(data) {
    var obj = [];

    const { id,idMemberCreator, type, date, } = data;
    const { translationKey, entities } = data.display;

    var dateTrello = moment(data.dateLastActivity);
    console.log(dateTrello);
    
    var actualDay = moment();
    var days = actualDay.diff(dateTrello, 'days');

    
    const member = { id, idMemberCreator, type, date, translationKey, entities };
    obj.push(member);


    return obj;
}

function actionIdBoard(data) {

    const { 
        id, name, closed, 
        idOrganization, 
        pinned, starred, 
        memberships, dateLastActivity
         } =  data
     
    const response = {
        id, name, closed, 
        idOrganization, 
        pinned, starred,
        dateLastActivity, 
        memberships 
    }  

    return response;
}

function actionCard(data) {
    const obj = {};

    obj.id = data.id;
    obj.name = data.name;
    obj.closed = data.closed;
    obj.dueComplete = data.dueComplete;
    obj.dateLastActivity = data.dateLastActivity; 
    const { due, dueComplete } = data.badges;
    obj.badges = {due, dueComplete};

    return obj;
}

module.exports = {
    KEY,
    TOKEN,
    actionIdBoard,
    actionCard,
    action
};
