'use strict'

function KEY() {
    return "24308d2ea050461c8e189707086a6915";
}

function TOKEN() {
    return "4c22fa6749c5d3f7fea358c4ec980739bb9e2d01cad596031d0b6d3c250b2310";
}

function checklistsCard(data) {
    const obj = {};
    var checkItems = [];

    obj.id = data.id;
    obj.name = data.name;
    obj.idBoard = data.idBoard;
    obj.idCard = data.idCard;
    for (let task of data.checkItems) {
        const { idChecklist, state, id, name } = task;
        checkItems.push({ idChecklist, state, id, name });
    }
    obj.checkItems = checkItems;
    
    return obj;
}

function InformationCard(data) {
    const obj = {};
    var badges = [];
    obj.id = data.id;
    const { checkItems, checkItemsChecked, dueComplete} = data.badges;
    badges.push({ checkItems, checkItemsChecked, dueComplete });
    obj.badges = badges;
    obj.closed = data.closed;
    obj.dueComplete = data.dueComplete;
    obj.dateLastActivity = data.dateLastActivity;
    
    return obj;
}

function CardBoard(data) {
    const obj = {};

    obj.id = data.id;
    obj.name = data.name;
    obj.closed = data.closed;
    obj.dateLastActivity = data.dateLastActivity;
    obj.memberships = data.memberships;

    return obj;
}

module.exports = {
    KEY,
    TOKEN,
    checklistsCard,
    InformationCard,
    CardBoard
}