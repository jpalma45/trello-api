'use strict'

var moment = require('moment');

function KEY() {
    return "24308d2ea050461c8e189707086a6915";
}

function TOKEN() {
    return "4c22fa6749c5d3f7fea358c4ec980739bb9e2d01cad596031d0b6d3c250b2310";
}

function GetlistActionsInfo(data) {
    const obj = {};
  
    obj.id = data.id;
    obj.type = data.type;
    
    var dateTrello = moment(data.date);
    var actualDay = moment();
    var days = actualDay.diff(dateTrello, 'days');

    obj.daysInList = days;
    obj.date = data.date;
    obj.idMemberCreator = data.idMemberCreator;
    obj.memberCreator = [{
        activityBlocked:  data.memberCreator.activityBlocked,
        fullName : data.memberCreator.fullName
    }];

    return obj;
  }

function GetlistCardsInfo(data) {
    const obj = {};

    obj.id = data.id;
    obj.checkItemStates = data.checkItemStates;
    obj.closed = data.closed;
    obj.dateLastActivity = data.dateLastActivity;
    obj.idBoard = data.idBoard;
    obj.idList = data.idList;

    return obj;
}

module.exports = {
    KEY,
    TOKEN,
    GetlistActionsInfo,
    GetlistCardsInfo
}